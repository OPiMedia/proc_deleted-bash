#!/bin/bash

#
# Usage: TEST_proc_deleted.sh
#
# Runs some temporary processes with 2 associated files,
# deletes all these files,
# and then runs several times ./proc_deleted with some options to check it.
#
# November 19, 2023
#
# GPLv3 --- Copyright (C) 2021-2023 Olivier Pirson
# http://www.opimedia.be/
#

PREFIX='TEST_proc_deleted'
OUTPUT="${PREFIX}_output.txt"

{  # Write stdout and stderr to "$OUTPUT"
    SUFFIXES='X1 Y1 Y2'

    unset PROC_DELETED_ARGS


    echo "Test results of proc_deleted --- $(date)"


    for FILE in proc_deleted TEST_proc_deleted.sh TEST_run_sleeps.sh; do
        echo '----------'
        printf '$ shellcheck %s\n' "$FILE"
        shellcheck "$FILE"
    done


    declare -i SLEEP=1000


    echo '----------'
    echo 'Runs processes with current working directory and files'

    # For /proc/*/cwd
    mkdir --parents TEST_proc_deleted__cwd
    cd TEST_proc_deleted__cwd || exit 1
    sleep $SLEEP &
    cd - || exit 1

    # For /proc/*/exe
    cp /bin/sleep TEST_proc_deleted__sleep
    ./TEST_proc_deleted__sleep 10m &

    # For /proc/*/fd/*
    for SUFFIX in $SUFFIXES; do
        F1="${PREFIX}_TMP_$SUFFIX.txt"
        F2="${PREFIX}_TMP_${SUFFIX}_err.txt"
        SLEEP=$((SLEEP + 1))
        sleep $SLEEP > "$F1" 2> "$F2" &
    done


    sleep 0.5

    echo '----------'
    echo 'Deletes their directory and files'

    rmdir TEST_proc_deleted__cwd

    rm TEST_proc_deleted__sleep

    for SUFFIX in $SUFFIXES; do
        F1="${PREFIX}_TMP_$SUFFIX.txt"
        F2="${PREFIX}_TMP_${SUFFIX}_err.txt"
        rm "$F1" "$F2"
    done


    for OPTIONS in '--version' \
                       '--help' \
                       \
                       "${PREFIX}" \
                       "${PREFIX} Y[12]\\.\\(txt\\)" \
                       "${PREFIX} Y[12]\\.(txt) --extended-regexp" \
                       "${PREFIX} Y \\.txt --fixed-strings" \
                       "${PREFIX} Y .txt --fixed-strings" \
                       \
                       "   ${PREFIX}    [XY]1(_err)?\\.txt --extended-regexp -V [Y]1(_err)?\\.txt" \
                       "-V ${PREFIX} -V [XY]1(_err)?\\.txt --extended-regexp    [Y]1(_err)?\\.txt --invert-match" \
                       \
                       "${PREFIX} Y1.txt --fixed-strings" \
                       "${PREFIX} ---debug-options Y1.txt --fixed-strings -x" \
                       \
                       "${PREFIX} Y --arguments" \
                       \
                       "${PREFIX} Y --no-list -d ," \
                       "${PREFIX} Y --no-list" \
                       "${PREFIX} Y --no-list --ps" \
                       "${PREFIX} --no-list --ps" \
                       \
                       "${PREFIX} Y -t" \
                       "${PREFIX} Y -t --reverse" \
                       \
                       "${PREFIX} Y --table" \
                       \
                       "${PREFIX}" \
                       '==ask==' \
                       "${PREFIX} --kill --no-ask" \
                       "${PREFIX}" \
                   ; do
        sleep 0.5
        echo '----------'
        if [[ "$OPTIONS" == '==ask==' ]]; then
            finished.sh --beep --no-config --transient 2> /dev/null  # https://bitbucket.org/OPiMedia/etoolbox
            read -n 1 -r -p 'The list above it is correct? If yes, then will run with --kill --no-ask. [y/N] '
            echo
            if [[ ! ("$REPLY" =~ ^[Yy]$) ]]; then
                exit 1
            fi
        else
            printf '$ ./proc_deleted %s\n' "$OPTIONS"

            # shellcheck disable=SC2086  # double quotes
            ./proc_deleted $OPTIONS
        fi
    done

    echo '---------- END ----------'
} 2>&1 | tee "$OUTPUT"


# Clean "noise" in the output result
sed -E --in-place \
    -e 's|^/home/.+?(/proc_deleted-bash)|fake_path\1|g' \
    -e 's|(-> )/.+?(/proc_deleted-bash/)|\1fake_path\2|g' \
    -e 's/ [[:alpha:]]{3} {1,2}[[:digit:]]{1,2} {1,2}[[:digit:]]{2}:[[:digit:]]{2} / fake_date_time /g' \
    -e 's/ [[:digit:]]{2}:[[:digit:]]{2} / fake_time /g' \
    -e "s| pts/[[:digit:]] | pts/0 |g" \
    -e "s/ $USER / fake_user /g" \
    "$OUTPUT"


declare -a PIDS

read -d '\n' -r -a PIDS < <(grep --extended-regexp '/proc/[[:digit:]]+/' "$OUTPUT" |
                                sed -E -e 's|^.*/proc/([[:digit:]]+)/.*$|\1|' |
                                sort --unique)

declare -a SED_OPTIONS
declare -i I=100001

for PID in "${PIDS[@]}"; do
    SED_OPTIONS+=(-e "s/$PID(\s+)[[:digit:]]+(\s+)0/$I\1100000\20/g"  # replace PID and PPID in ps result
                  -e "s/$PID/$I/g")  # replace PID elsewhere
    I=$((I + 1))
done

sed -E --in-place "${SED_OPTIONS[@]}" "$OUTPUT"


# Compare results
echo "COMPARE $OUTPUT and TEST_proc_deleted_correct_output.txt"
if diff --color <(tail --lines +2 "$OUTPUT") <(tail --lines +2 TEST_proc_deleted_correct_output.txt); then
    echo "$(tput setaf 2)OK$(tput sgr0)"
else
    echo "$(tput setaf 1)DIFFERENT$(tput sgr0)"
fi
