#!/bin/bash

#
# Usage: TEST_run_sleeps.sh [--only-create] [TEST_DIR]
#
# Runs 3 sleep processes
# and then deletes associated current working directory, executable file and file.
#
# November 6, 2023
#
# GPLv3 --- Copyright (C) 2023 Olivier Pirson
# http://www.opimedia.be/
#


# Reads arguments
declare -i IS_ONLY_CREATE=1
TEST_DIR=/tmp

while [[ $# -ne 0 ]]; do
    case "$1" in
        --only-create)
            IS_ONLY_CREATE=0
            ;;
        -*)
            exit 1
            TEST_DIR="$1"
            ;;
    esac
    shift
done


# For /proc/*/cwd
DIR="$TEST_DIR/proc_deleted__dir"

echo "Create directory $DIR"
mkdir -p "$DIR"
cd "$DIR" || exit 1
sleep 1000 &
cd - || exit 1
rmdir "$DIR"


# For /proc/*/exe
EXE="$TEST_DIR/proc_deleted__sleep_exe"
echo "Create executable $EXE"
cp "$(which sleep)" "$EXE"
"$EXE" 1001 &
rm "$EXE"


# For /proc/*/fd/*
FILE_OUT="$TEST_DIR/proc_deleted__file_out"
FILE_ERR="$TEST_DIR/proc_deleted__file_err"
echo "Create files $FILE_OUT and $FILE_ERR"
sleep 1002 > "$FILE_OUT" 2> "$FILE_ERR" &
sleep 0.5  # to be sure that files are already created
rm "$FILE_OUT" "$FILE_ERR"


# Runs proc_deleted
if [[ $IS_ONLY_CREATE -ne 0 ]]; then
    ./proc_deleted proc_deleted --ps --arguments --kill
fi
