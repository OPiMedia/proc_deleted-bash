#!/bin/bash

#
# Create example to take screenshots
#
# October 22, 2023
#
# GPLv3 --- Copyright (C) 2023 Olivier Pirson
# http://www.opimedia.be/
#

PROC_DELETED=../../../proc_deleted


# Create processes
../../../TEST_run_sleeps.sh --only-create


# Build data
echo   '$ proc_deleted --table sleep --ps' > proc_deleted__out--ps.out
unbuffer $PROC_DELETED --table sleep --ps >> proc_deleted__out--ps.out 2> proc_deleted__out--ps.err

echo   '$ proc_deleted sleep --no-list' > proc_deleted__out--no-list.out
unbuffer $PROC_DELETED sleep --no-list >> proc_deleted__out--no-list.out 2> proc_deleted__out--no-list.err

echo   '$ proc_deleted --table file_out --ps -x' > proc_deleted__out--ps-x.out
unbuffer $PROC_DELETED --table file_out --ps -x >> proc_deleted__out--ps-x.out 2> proc_deleted__out--ps-x.err


# Cleaning
$PROC_DELETED proc_deleted --ps --params --kill


# Display to take screenshots
echo '===== DISPLAY of examples ====='
for FILE in proc_deleted__out*.out; do
    echo
    cat "$FILE"
done
echo
