# proc_deleted (Bash)
Bash command to **list processes that have some deleted files**,
or for which their current working directory is deleted,
and to allow to **kill them**.
Also detect processes for which the executable file is deleted.


More precisely, takes list of `/proc/*/cwd`, `/proc/*/exe` and `/proc/*/fd/*`
with mention `"(deleted)"` or when they are missing.
Keeps only lines that **match all PATTERNS** (regular expression like with grep).
Extracts PIDs, and then displays the list and PIDs.
Options allow to kill selected processes.



## Examples of use
Lists all `/proc/*/cwd`, `/proc/*/exe` and `/proc/*/fd/*` with mention `"(deleted)"` and lists their PIDs:

```bash
$ proc_deleted
```

Lists lines that match both regular expressions `tmp` *and* `#[0-5]`, and lists their PIDs:

```bash
$ proc_deleted tmp '#[0-5]'
```

Lists lines that match the regular expression `tmp`, and lists their PIDs,
asks confirmation and then runs `kill` command on PIDs selected:

```bash
$ proc_deleted tmp --kill
```

Only lists PIDs of lines that match the regular expression `tmp`, separated by `','` instead `' '`:
```bash
$ proc_deleted tmp --no-list -d ,
```

Shows use and all options:

```bash
$ proc_deleted --help
```

**⚠ WARNING!**

Don't kill processes if you are not sure what you are doing.

Don't make a confusion.
A deleted file associated with a running process does not necessarily mean that this process has a problem.


`$ proc_deleted --table sleep --ps`  
![proc_deleted _out --ps](https://bitbucket.org/OPiMedia/proc_deleted-bash/raw/master/_img/screenshots/proc_deleted__out--ps.png)

`$ proc_deleted _out --ps -x`
(`'_out'` doesn't match the second line, but it is the same PID that the first line)  
![proc_deleted _out --ps -x](https://bitbucket.org/OPiMedia/proc_deleted-bash/raw/master/_img/screenshots/proc_deleted__out--ps-x.png)

`$ proc_deleted sleep --no-list`  
![proc_deleted _out --ps](https://bitbucket.org/OPiMedia/proc_deleted-bash/raw/master/_img/screenshots/proc_deleted__out--no-list.png)



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/workspace/repositories>

- 📧 <olivier.pirson.opi@gmail.com>
- 🧑‍🤝‍🧑 Bluesky: <https://bsky.app/profile/opimedia.bsky.social> — Mastodon: <https://mamot.fr/@OPiMedia> — X/Twitter: <https://twitter.com/OPirson>
- 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
- other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png



## Support me
This program is a **free software** (GPL license).
It is **completely free** (like "free speech" *and like "free beer").
However you can **support me** financially by donating.

Click to this link
[![Donate](http://www.opimedia.be/_png/donate--76x47-t.png)](http://www.opimedia.be/donate/)
**Thank you!**



## License: [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) ![GPLv3](https://www.gnu.org/graphics/gplv3-88x31.png)
Copyright (C) 2021-2023 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.



## Changes
* November 19, 2023: Minor cleaning.
* November 13, 2023: Minor cleaning.
* November 12, 2023: Minor cleaning.
* November 11, 2023: Few cleaning.
* November 6, 2023: Renamed `--params` option to `--arguments`.
* November 5, 2023: Few cleaning.
* November 2, 2023:

    - Improved options checking.
    - Few cleaning.

* October 22, 2023:

    - Added option `-V PATTERN`.
    - Added usage of the environment variable `PROC_DELETED_ARGS`.

* October 21, 2023:

    - Added result from `/proc/*/cwd`.
    - Added result with missing `/proc/*/cwd`, `/proc/*/exe` or `/proc/*/fd/`.

* October 17, 2023: Added `TEST_run_sleeps.sh`.
* August 26, 2023: Renamed `proc_deleted.sh` to `proc_deleted`.
* July 21, 2023: Added note about top -p limitation.
* October 9, 2022:

    - Added result from `/proc/*/exe`.
    - Added checking that processes running yet.
    - Replaced use of `ps` to find associated commands by `/proc/PID/cmdline`.
    - Replaced use of big strings by arrays.

* June 23, 2022: Added options `-r`, `--reverse` and `-t`.
* December 7, 2021: Corrected bug of PIDs list for `htop` and `top`.
* December 5, 2021: Corrected bug with empty list.
* November 16, 2021: Added option `--params` (now `--arguments`).
* November 14, 2021: Improved some `<<<` redirections.
* November 13, 2021: Cleaned `IFS` and some ${pids[*]} expansions.
* November 4, 2021: Corrected typos.
* November 2, 2021:

    - Added option `---debug-options`.
    - Encapsulated local variable and functions to main function.

* October 30, 2021: Set global constants as read only.
* October 23, 2021: Simplified extraction of PID to avoid columns problem.
* October 20, 2021: Added warning in help message and README.
* October 18, 2021: Added display of command name, and option `--table`.
* October 17, 2021: Added option `-d DELIM`.
* October 16, 2021: First public version.
* Started October 14, 2021.



![proc_deleted](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2021/Oct/16/1662249087-6-proc_deleted-bash-logo_avatar.png)

#proc_deleted



## Other personal Bash free softwares
* [etoolbox](https://bitbucket.org/OPiMedia/etoolbox/):
  Various scripts.
* [gocd (Bash)](https://bitbucket.org/OPiMedia/gocd-bash/):
  Change working directory from a list of association `NAME: DIRECTORY`, with autocompletion.
* [HackerRank [CodinGame…] / helpers](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/):
  Help in solving problems of HackerRank website (or CodinGame…), in several programming languages.
* [vNu-scripts](https://bitbucket.org/OPiMedia/vnu-scripts/):
  Shell scripts to run *the Nu Html Checker (v.Nu)* to valid (X)HTML files by identifying if each file is a (X)HTML file and which version of (X)HTML.
